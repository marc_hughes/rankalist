TIMESTAMP=`date +%s`
echo $TIMESTAMP

rm -rf static-build
python rankalist_web/manage.py collectstatic --noinput

./s3copy.rb -c js -c css -t -z -r -p -x $TIMESTAMP/ -b rankalist-cdn static-build

rm static-build/client/assets/fonts/*
find static-build -name "*.psd" -exec rm -rf {} \;

heroku config:add STATIC_URL=http://s3.amazonaws.com/rankalist-cdn/$TIMESTAMP/static-build/

