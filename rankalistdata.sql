--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('auth_message_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('auth_permission_id_seq', 39, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('django_content_type_id_seq', 13, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Name: rankalist_list_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('rankalist_list_id_seq', 54, true);


--
-- Name: rankalist_listitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('rankalist_listitem_id_seq', 446, true);


--
-- Name: rankalist_vote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('rankalist_vote_id_seq', 616, true);


--
-- Name: rankalist_voter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('rankalist_voter_id_seq', 73, true);


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rankalist
--

SELECT pg_catalog.setval('south_migrationhistory_id_seq', 4, true);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: rankalist
--



--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO django_content_type (id, name, app_label, model) VALUES (1, 'permission', 'auth', 'permission');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (2, 'group', 'auth', 'group');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (3, 'user', 'auth', 'user');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (4, 'message', 'auth', 'message');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (5, 'content type', 'contenttypes', 'contenttype');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (6, 'session', 'sessions', 'session');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (7, 'site', 'sites', 'site');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (8, 'log entry', 'admin', 'logentry');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (9, 'migration history', 'south', 'migrationhistory');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (10, 'list', 'rankalist', 'list');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (11, 'list item', 'rankalist', 'listitem');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (12, 'voter', 'rankalist', 'voter');
INSERT INTO django_content_type (id, name, app_label, model) VALUES (13, 'vote', 'rankalist', 'vote');


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (1, 'Can add permission', 1, 'add_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (2, 'Can change permission', 1, 'change_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (3, 'Can delete permission', 1, 'delete_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (4, 'Can add group', 2, 'add_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (5, 'Can change group', 2, 'change_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (6, 'Can delete group', 2, 'delete_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (7, 'Can add user', 3, 'add_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (8, 'Can change user', 3, 'change_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (9, 'Can delete user', 3, 'delete_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (10, 'Can add message', 4, 'add_message');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (11, 'Can change message', 4, 'change_message');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (12, 'Can delete message', 4, 'delete_message');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (13, 'Can add content type', 5, 'add_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (14, 'Can change content type', 5, 'change_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (15, 'Can delete content type', 5, 'delete_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (16, 'Can add session', 6, 'add_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (17, 'Can change session', 6, 'change_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (18, 'Can delete session', 6, 'delete_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (19, 'Can add site', 7, 'add_site');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (20, 'Can change site', 7, 'change_site');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (21, 'Can delete site', 7, 'delete_site');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (22, 'Can add log entry', 8, 'add_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (23, 'Can change log entry', 8, 'change_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (24, 'Can delete log entry', 8, 'delete_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (25, 'Can add migration history', 9, 'add_migrationhistory');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (26, 'Can change migration history', 9, 'change_migrationhistory');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (27, 'Can delete migration history', 9, 'delete_migrationhistory');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (28, 'Can add list', 10, 'add_list');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (29, 'Can change list', 10, 'change_list');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (30, 'Can delete list', 10, 'delete_list');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (31, 'Can add list item', 11, 'add_listitem');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (32, 'Can change list item', 11, 'change_listitem');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (33, 'Can delete list item', 11, 'delete_listitem');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (34, 'Can add voter', 12, 'add_voter');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (35, 'Can change voter', 12, 'change_voter');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (36, 'Can delete voter', 12, 'delete_voter');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (37, 'Can add vote', 13, 'add_vote');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (38, 'Can change vote', 13, 'change_vote');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (39, 'Can delete vote', 13, 'delete_vote');


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: rankalist
--



--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO auth_user (id, username, first_name, last_name, email, password, is_staff, is_active, is_superuser, last_login, date_joined) VALUES (1, 'mhughes', '', '', 'marc.hughes@gmail.com', 'sha1$7d0ed$a9a4031ad1922188a621de13e4dae9aacd5195c3', true, true, true, '2012-01-20 20:43:45.051562-05', '2011-10-12 15:18:16.304425-04');


--
-- Data for Name: auth_message; Type: TABLE DATA; Schema: public; Owner: rankalist
--



--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: rankalist
--



--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: rankalist
--



--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) VALUES (1, '2011-10-14 12:40:54.412377-04', 1, 12, '15', 'Voter object', 3, '');


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('a7265a736a9763054a557be1a61460bd', 'NWVmZjA5YmQ3MGU2MGFiNDA4MTYzY2U3OTQwOTJhZDlkMzNiMDExNTqAAn1xAShVEl9hdXRoX3Vz
ZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED
VQ1fYXV0aF91c2VyX2lkcQRLAXUu
', '2011-10-26 18:06:32.463512-04');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('1dd1c71f0ce8e5e63aadbec7b1a666ed', 'NWVmZjA5YmQ3MGU2MGFiNDA4MTYzY2U3OTQwOTJhZDlkMzNiMDExNTqAAn1xAShVEl9hdXRoX3Vz
ZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED
VQ1fYXV0aF91c2VyX2lkcQRLAXUu
', '2011-10-26 20:32:51.535308-04');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('844d99d57d983dd3eae794db605f266f', 'NWVmZjA5YmQ3MGU2MGFiNDA4MTYzY2U3OTQwOTJhZDlkMzNiMDExNTqAAn1xAShVEl9hdXRoX3Vz
ZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED
VQ1fYXV0aF91c2VyX2lkcQRLAXUu
', '2012-02-03 20:43:45.318792-05');


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO django_site (id, domain, name) VALUES (1, 'example.com', 'example.com');


--
-- Data for Name: rankalist_list; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (1, 'g7F7LYkk', 'ZubQ2PSr', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (2, 'UuynM0q3', 'y0FN7cYv', 'Here''s an example!  This text you''re reading now was entered into the "instructions" field.  What''s the best part about RankAList?');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (3, '7uvPpZjF', 'cmycSFI7', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (4, 'iYMAvSE9', 'u62YDpk2', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (5, 'O1zeQqPi', 'uUiNPyI8', 'Please rank these items in order of customer importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (6, 'UOm0pcN9', '5kS7vmXa', 'Please rank these items in order of customer importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (7, 'zR35XYmj', 'gTNWHVns', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (8, 'GgEKeb9K', 'Yrj6euSW', 'Please rank these items in order of importance to the customer for the first release of the Math 180 product.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (9, 'QxsjCXTz', 'bVKKanSd', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (10, 'sqmcM3DA', 'KwYyLLZw', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (11, 'Qiaw5iSH', '0qfVeUeq', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (12, 'niuoOfyt', 'HMkbnSgu', 'Please rank these items in order of importance to the customer for the first release of Math 180.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (13, 'm32uMPL3', 'hnDpRKXi', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (14, 'gIgIngfH', 'xw5tkO7W', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (15, 'vvVWoMww', 'IAkl295T', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (16, '9QVuao9f', 'BZnRh0Yt', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (17, 'D6FBBnKL', 'ABq35xEQ', 'rank this');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (18, 'uk9qzurN', 'g6Ucbc3b', 'asdfasdfasdf
asdfas
dfas
dfas
df
asdfasdfasdf¸');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (19, '0RF4E5lI', 'DnrKNlop', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (20, 'ntuN3mIe', 'lQzF7jtk', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (21, 'mHnFsv2o', '6VTk0Ftb', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (22, 'TUvYPCLT', 'o6d0Khto', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (23, '8xH7suUp', 'xQkzBWb7', 'Äpfel
Bananen
Erdbeeren');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (24, '0Vhf9MDa', 'CtFUZ7kr', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (25, 'DFPV1qFf', 'QuzmIT12', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (26, 'LIYAdR6i', '5KMKKeje', 'one');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (27, 't2WyGij9', '0o5saR0i', 'Welche Zeitung sollen wir für das W-Straßen Büro abonnieren? Bitte reiht Eure Lieblungszeitunten.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (28, 'nrUSj4Eh', 'wy2YUlCM', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (29, 'dTWgTgHq', 'uL6Szb5n', 'first, second');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (30, '9ihpv9uY', 'rv2NxTcf', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (31, 'jI0IOVul', 'CYlT866P', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (32, 'Y7n97a0p', 'T1yPyPYC', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (33, 'Moxo1i56', 'i0EEXuMt', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (34, 'LsBXLCyU', 'VWOuqcOA', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (35, 'CXOnG0QA', 'WSMqwJOm', 'Please arrange these options in order of the one you''d most want to play, to the one you''d least want to play.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (36, 'wVoyZ8mt', 'lu2W6HMO', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (37, 'RewayLbY', 'HcpCpiaq', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (38, 'tPs98lSF', 'IgXDT5hG', 'These are the top Blue Jays prospects according to John Sickels of Minor League Ball. How would you rank your favorite prospects?');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (39, 'LA05hGb8', 'h0bpXFT2', 'instructuions');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (40, 'NRlcvUCm', 'd93XeZpB', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (41, 'PNx0VI2q', 'IP0Dp9iL', 'Got it! Thanks a lot again for heplnig me out!');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (42, 'gAEmBMNO', 'h9l1BtGd', 'xGUvl8  <a href="http://jwuyxkaezdov.com/">jwuyxkaezdov</a>');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (43, 'wtwFFvV4', 'HiHvIx5J', 'rTHjmm , [url=http://uughsuxqnhjk.com/]uughsuxqnhjk[/url], [link=http://wriozgixcodn.com/]wriozgixcodn[/link], http://xkdnluweimes.com/');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (44, 'DBE39jY1', 'Gwkep70x', 'eOq4h4  <a href="http://hjeivglekikx.com/">hjeivglekikx</a>');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (45, 'DE0GQ7Je', 'Xs3Fo4De', 'uRmb3K , [url=http://ikvqjtfsofvi.com/]ikvqjtfsofvi[/url], [link=http://zawaubnjwdrj.com/]zawaubnjwdrj[/link], http://roicyhhumdii.com/');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (46, 'Wy4BHRNe', '3l4QkICW', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (47, 'u7Z1UEbH', 'q5GLDUu8', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (48, 'sk1iiuxv', '01GNEznG', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (49, 'gL1gMI8r', 'yu3NIA6K', 'Okay folks, it''s that time of year again (actually, it''s the first time).  We''re trying to come up with the 2011 Best of Jason''s Stupid Facebook Posts and need your help!  Please rank the following in terms of best to worst.  We''re just looking for the ''best'' - you decide what that means to you.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (50, 'C4cojJbI', 'pP3e4yaD', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (51, 'pFMUvBL4', 'LzPmHGuB', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (52, 'U8EdjVq0', '5EHbPfxr', 'What is the biggest impediment to working with VDSI');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (53, 'I5KSUmyW', 'Ehm3sHGD', 'Please rank these items in order of importance.');
INSERT INTO rankalist_list (id, key, "adminKey", instructions) VALUES (54, 'IxacpAcN', 'b26znuPG', 'Please rank these items in order of importance.');


--
-- Data for Name: rankalist_listitem; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (1, 1, 'MRI');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (2, 1, 'SMI Reports');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (3, 1, 'Technical Manuals/Documentation');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (4, 1, 'Demo');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (5, 1, 'Simulator');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (6, 1, 'Professional Development');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (7, 1, 'mSkills');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (8, 1, 'Student Software: M180 + Boot Camp');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (9, 1, 'Pupil Edition (digital version)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (10, 1, 'Teacher''s Edition (digital version)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (11, 1, 'Content-first system for Digital & Print books');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (12, 1, 'Family Portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (13, 1, 'Digital Teaching Environment (student-facing; for use in classroom)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (14, 1, 'Leader Dashboard');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (15, 1, 'Teacher Dashboard');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (16, 1, 'Student Dashboard');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (17, 2, 'The font!');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (18, 2, 'Forces people to rank lists absolutely');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (19, 2, 'No signing up');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (20, 2, 'Super easy to use');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (21, 3, 'k');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (22, 3, 'k');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (23, 3, 'm');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (24, 3, 'j');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (25, 3, 'n');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (26, 3, 'h');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (27, 3, 'b');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (28, 3, 'g');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (29, 3, 'r');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (30, 3, 'e');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (31, 3, 'd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (32, 3, 'a''d');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (33, 3, 'be');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (34, 4, 'Fix the front porch light');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (35, 4, 'Paint the office doors');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (36, 4, 'Shake the baby');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (37, 4, 'Build a wood shed');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (38, 4, 'Fix the bedroom ceiling');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (39, 5, 'Content First publishing system for digital + print');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (40, 5, 'Spanish audio recordings');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (41, 5, 'Software web demo for marketing / sales');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (42, 5, 'Software simulator for marketing / training');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (43, 5, 'Digital MRI - formative assessment Individual Interview');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (44, 5, 'Tablet versions of some BZ games');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (45, 5, 'Tablet companion app for teacher');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (46, 5, 'Digital mBook (full interactivity w/ recording data to SAM)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (47, 5, 'Digital mBook (as an e-book)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (48, 5, 'Leadership/Administrator dashboards');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (49, 5, 'A family / parent portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (50, 5, 'A tutor portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (51, 5, 'Professional development website');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (52, 5, 'Teacher dashboards - Out of class use by teacher for planning, and data snapshots');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (53, 5, 'Digitial teaching environment (for in class use by teacher. Includes IWB, classroom management, digital TE)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (54, 5, 'Success Zone - Zone to ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (55, 5, 'Challenge Zone - Zone to practice and extend the current topic (transfer tasks)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (56, 5, 'Practice Zone - Zone to maintain previously learnt topics');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (57, 5, 'Brain Zone Games #21-#27');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (58, 5, 'Brain Zone Games #11-#20');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (59, 5, 'Brain Zone Games #1-#10 ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (60, 5, 'Brain Zone - Zone to play games to gain fluency.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (61, 5, 'New SMI Reports / Integration - General placement and progress monitoring');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (62, 5, 'mSkills - Targeted assessment on specific M180 content');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (63, 5, 'SAM Reports');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (64, 5, 'Block intro videos');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (65, 5, 'Boot Camp Student Software Lessons');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (66, 5, 'Year 1 Student Software Lessons (Connect, Teach, Supported Practice)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (67, 6, 'Concept book');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (68, 6, 'Manipulatives');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (69, 6, 'Printed Pupil Edition ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (70, 6, 'Printed Teacher Edition');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (71, 6, 'Glossary ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (72, 6, 'Smart Content Delivery Algoritm to decide what content comes next');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (73, 6, 'Recorded voiceover audio (non-Spanish) ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (74, 6, 'Instructional videos (Teach, Reteach) ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (75, 6, 'Content First publishing system for digital + print');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (76, 6, 'Spanish audio recordings');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (77, 6, 'Software web demo for marketing / sales');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (78, 6, 'Software simulator for marketing / training');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (79, 6, 'Digital MRI - formative assessment Individual Interview');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (80, 6, 'Tablet versions of some BZ games');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (81, 6, 'Tablet companion app for teacher');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (82, 6, 'Digital mBook (full interactivity w/ recording data to SAM)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (83, 6, 'Digital mBook (as an e-book)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (84, 6, 'Leadership/Administrator dashboards');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (85, 6, 'A family / parent portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (86, 6, 'A tutor portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (87, 6, 'Professional development website');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (88, 6, 'Teacher dashboards - Out of class use by teacher for planning, and data snapshots');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (89, 6, 'Digitial teaching environment (for in class use by teacher. Includes IWB, classroom management, digital TE)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (90, 6, 'Success Zone - Zone to ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (91, 6, 'Challenge Zone - Zone to practice and extend the current topic (transfer tasks)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (92, 6, 'Practice Zone - Zone to maintain previously learnt topics');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (93, 6, 'Brain Zone Games #21-#27');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (94, 6, 'Brain Zone Games #11-#20');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (95, 6, 'Brain Zone Games #1-#10 ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (96, 6, 'Brain Zone - Zone to play games to gain fluency.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (97, 6, 'New SMI Reports / Integration - General placement and progress monitoring');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (98, 6, 'mSkills - Targeted assessment on specific M180 content');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (99, 6, 'SAM Reports');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (100, 6, 'Block intro videos');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (101, 6, 'Boot Camp Student Software Lessons');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (102, 6, 'Year 1 Student Software Lessons (Connect, Teach, Supported Practice)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (103, 7, 'd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (104, 7, 'c');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (105, 7, 'b');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (106, 7, 'a');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (107, 8, 'Concept book');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (108, 8, 'Manipulatives');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (109, 8, 'Printed Pupil Edition ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (110, 8, 'Printed Teacher Edition');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (111, 8, 'Glossary ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (112, 8, 'Smart Content Delivery Algoritm to decide what content comes next');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (113, 8, 'Recorded voiceover audio (non-Spanish) ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (114, 8, 'Instructional videos (Teach, Reteach) ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (235, 20, 'G');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (236, 20, 'F');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (115, 8, 'Content First publishing system for digital + print');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (116, 8, 'Spanish audio recordings');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (117, 8, 'Software web demo for marketing / sales');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (118, 8, 'Software simulator for marketing / training');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (119, 8, 'Digital MRI - formative assessment Individual Interview');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (120, 8, 'Tablet versions of some BZ games');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (121, 8, 'Tablet companion app for teacher');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (122, 8, 'Digital mBook (full interactivity w/ recording data to SAM)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (123, 8, 'Digital mBook (as an e-book)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (124, 8, 'Leadership/Administrator dashboards');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (125, 8, 'A family / parent portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (126, 8, 'A tutor portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (127, 8, 'Professional development website');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (128, 8, 'Teacher dashboards - Out of class use by teacher for planning, and data snapshots');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (129, 8, 'Digitial teaching environment (for in class use by teacher. Includes IWB, classroom management, digital TE)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (130, 8, 'Success Zone - Zone to ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (131, 8, 'Challenge Zone - Zone to practice and extend the current topic (transfer tasks)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (132, 8, 'Practice Zone - Zone to maintain previously learnt topics');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (133, 8, 'Brain Zone Games #21-#27');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (134, 8, 'Brain Zone Games #11-#20');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (135, 8, 'Brain Zone Games #1-#10 ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (136, 8, 'Brain Zone - Zone to play games to gain fluency.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (137, 8, 'New SMI Reports / Integration - General placement and progress monitoring');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (138, 8, 'mSkills - Targeted assessment on specific M180 content');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (139, 8, 'SAM Reports');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (140, 8, 'Block intro videos');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (141, 8, 'Boot Camp Student Software Lessons');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (142, 8, 'Year 1 Student Software Lessons (Connect, Teach, Supported Practice)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (143, 9, 'test 2');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (144, 9, 'BB touch');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (145, 9, 'Caching and merge into new lego structure.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (146, 9, 'Adcode change.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (147, 10, 'three');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (148, 10, 'two');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (149, 10, 'one');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (150, 11, 'asd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (151, 11, 'qwe');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (152, 11, 'asd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (153, 11, 'qwe');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (154, 11, 'asd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (155, 11, 'qwe');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (156, 11, 'asd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (157, 12, 'Physical Manipulatives (for in class use)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (158, 12, 'Digital Concept book');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (159, 12, 'Printed Concept book');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (160, 12, 'Printed Pupil Edition ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (161, 12, 'Printed Teacher Edition');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (162, 12, 'Glossary ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (163, 12, 'Smart Content Delivery Algorithm to decide what content comes next');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (164, 12, 'Content First publishing system for digital + print');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (165, 12, 'Recorded voiceover audio (Spanish)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (166, 12, 'Recorded voiceover audio (English) ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (167, 12, 'Software web demo for marketing / sales');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (168, 12, 'Software simulator for marketing / training');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (169, 12, 'Digital MRI - formative assessment Individual Interview');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (170, 12, 'Tablet versions of some BZ games');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (171, 12, 'Tablet companion app for teacher');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (172, 12, 'Digital mBook (full interactivity w/ recording data to SAM)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (173, 12, 'Digital mBook (as an e-book)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (174, 12, 'Leadership/Administrator dashboards');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (175, 12, 'A family / parent portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (176, 12, 'A tutor portal');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (177, 12, 'Professional development website');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (178, 12, 'Teacher dashboards - Out of class use by teacher for planning, and data snapshots');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (179, 12, 'Digitial teaching environment (for in class use by teacher. Includes IWB, classroom management, digital TE)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (180, 12, 'Success Zone  ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (181, 12, 'Challenge Zone - Zone to practice and extend the current topic (transfer tasks)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (182, 12, 'Practice Zone - Zone to maintain previously learnt topics');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (183, 12, 'Brain Zone Games #21-#27');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (184, 12, 'Brain Zone Games #11-#20');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (185, 12, 'Brain Zone Games #1-#10 ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (186, 12, 'Brain Zone - Zone to play games to gain fluency.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (187, 12, 'New SMI Reports / Integration - General placement and progress monitoring');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (188, 12, 'mSkills - Targeted assessment on specific M180 content');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (189, 12, 'SAM Reports');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (190, 12, 'Block intro videos');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (191, 12, 'Instructional videos (Teach, Reteach) ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (192, 12, 'Boot Camp Student Software Lessons');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (193, 12, 'Year 1 Student Software Lessons (Connect, Supported Practice)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (194, 13, '5. Photo editing');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (195, 13, '4. Sound design');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (196, 13, '3. Video promos');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (197, 13, '2. Mock-ups for site');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (198, 13, '1. T-shirt designs');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (199, 14, '5. Photo editing');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (200, 14, '4. Sound design');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (201, 14, '3. Video promos');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (202, 14, '2. Mock-ups for site');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (203, 14, '1. T-shirt designs');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (204, 15, 'asd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (205, 16, 'item c');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (206, 16, 'item b');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (207, 16, 'item a');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (208, 17, 'Add to task colour labels');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (209, 17, 'Add to task a check list');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (210, 17, 'Add to task people');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (211, 17, 'Activity viewer');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (212, 17, 'Drag and drop ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (213, 17, 'Commenting');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (214, 17, 'Overview Layout');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (215, 17, 'Multiple boards / projects');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (216, 18, 'rthet');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (217, 18, 'rtjh');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (218, 18, 'wth');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (219, 18, 'tth');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (220, 18, 'wrt');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (221, 18, '');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (222, 18, 'etzj');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (223, 18, 'ertwgzerjh');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (224, 18, 'qwertqwertqw');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (225, 18, 'wertweritgwert');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (226, 18, 'zwrzg');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (227, 18, 'dfghdfgt');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (228, 18, 'dfgdfdfg');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (229, 18, '');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (230, 18, 'asdfhsdf');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (231, 18, 'asdfgasdfgh');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (232, 18, 'asdg');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (233, 19, 'Bf dsfkdskf');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (234, 19, 'A  fdfjjds');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (237, 20, 'E');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (238, 20, 'D');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (239, 20, 'C');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (240, 20, 'B');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (241, 20, 'A');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (242, 21, 'fgdfgd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (243, 21, 'fgd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (244, 21, 'gd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (245, 21, 'dfgdf dfgdgdf');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (246, 22, 'aaaa');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (247, 23, 'Herd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (248, 23, 'Deckel');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (249, 23, 'Topf');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (250, 24, 'test3');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (251, 24, 'test2');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (252, 24, 'test1');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (253, 25, '123');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (254, 26, 'one');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (255, 27, 'Nürtinger Zeitung');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (256, 27, 'Handelsblatt');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (257, 27, 'Titanic');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (258, 27, 'Stuttgarter Zeitung');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (259, 27, 'Schwäbisches Tagblatt');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (260, 27, 'FAZ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (261, 27, 'Süddeutsche');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (262, 28, 'd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (263, 28, 'c');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (264, 28, 'b');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (265, 28, 'a');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (266, 29, 'SecondItem');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (267, 29, 'firstItem');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (268, 30, 'test3');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (269, 30, 'test2');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (270, 30, 'test1');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (271, 31, 'Test');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (272, 31, 'A');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (273, 31, 'Is');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (274, 31, 'This');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (275, 32, 'dre');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (276, 32, 'one');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (277, 32, 'tow');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (278, 32, 'the ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (279, 33, 'read terms');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (280, 33, 'read thesauri');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (281, 33, 'post thesauri');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (282, 33, 'post term for thesauri');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (283, 34, 'effect on bottom line');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (284, 34, 'strategic importance');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (285, 34, 'company');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (286, 34, 'title');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (287, 34, 'age');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (288, 34, 'picture');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (289, 34, 'name');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (290, 34, 'quote');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (291, 34, 'tagline');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (292, 34, 'philosophy of use');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (293, 34, 'satisfiers');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (294, 34, 'bottlenecks');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (295, 34, 'single point of failure');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (296, 34, 'influence');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (297, 34, 'share of base');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (298, 34, 'rate of use');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (299, 34, 'level of engagement');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (300, 34, 'uncommon interface');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (301, 34, 'secondary interface');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (302, 34, 'primary interface');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (303, 35, 'Setting E - Generic D&D Fantasy');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (304, 35, 'Setting D - Dark Sun');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (305, 35, 'Setting C - Arcane Punk');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (306, 35, 'Setting B - Fantasy (minor Science Fiction)');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (307, 35, 'Setting A - Mythology');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (308, 36, 's');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (309, 37, 'MNO');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (310, 37, 'JKL');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (311, 37, 'DEF');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (312, 37, 'GHI');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (313, 37, 'ABC');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (314, 38, 'Chino Vega');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (315, 38, 'Dickie Thon');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (316, 38, 'Mitchell Taylor');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (317, 38, 'John Stilson');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (318, 38, 'Tom Robson');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (319, 38, 'Sean Nolin');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (320, 38, 'Santiago Nessy');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (321, 38, 'Griffin Murphy');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (322, 38, 'Michael McDade');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (323, 38, 'Christian Lopes');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (324, 38, 'Chris Hawkins');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (325, 38, 'Jeremy Gabryszwski');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (326, 38, 'Anthony DeSclafini');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (327, 38, 'Evan Crawford');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (328, 38, 'David Cooper');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (329, 38, 'Kevin Comer');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (330, 38, 'Mark Biggs');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (331, 38, 'Danny Barnes');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (332, 38, 'Eric Arce');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (333, 38, 'Roberto Osuna');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (334, 38, 'Michael Crouse');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (335, 38, 'Moises Sierra');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (336, 38, 'Marcus Knecht');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (337, 38, 'Carlos Perez');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (338, 38, 'Chad Jenkins');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (339, 38, 'Joel Carreno');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (340, 38, 'Adeiny Hechavarria');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (341, 38, 'Matt Dean');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (342, 38, 'Joe Musgrove');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (343, 38, 'Dwight Smith, Jr');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (344, 38, 'Jacob Anderson');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (345, 38, 'Adonys Cardona');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (346, 38, 'Asher Wojciechowski');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (347, 38, 'Aaron Sanchez');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (348, 38, 'A.J. Jimenez');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (349, 38, 'Anthony Gose');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (350, 38, 'Drew Hutchison');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (351, 38, 'Deck McGuire');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (352, 38, 'Daniel Norris');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (353, 38, 'Justin Nicolino');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (354, 38, 'Noah Syndergaard');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (355, 38, 'Jake Marisnick');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (356, 38, 'Nestor Molina');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (357, 38, 'Travis D''Arnaud');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (358, 39, 'item');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (359, 40, 'sdsd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (360, 40, 'sdsds');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (361, 40, 'ffff');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (362, 41, 'Got it! Thanks a lot again for heplnig me out!');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (363, 42, 'xGUvl8  <a href="http://jwuyxkaezdov.com/">jwuyxkaezdov</a>');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (364, 43, 'rTHjmm , [url=http://uughsuxqnhjk.com/]uughsuxqnhjk[/url], [link=http://wriozgixcodn.com/]wriozgixcodn[/link], http://xkdnluweimes.com/');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (365, 44, 'eOq4h4  <a href="http://hjeivglekikx.com/">hjeivglekikx</a>');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (366, 45, 'uRmb3K , [url=http://ikvqjtfsofvi.com/]ikvqjtfsofvi[/url], [link=http://zawaubnjwdrj.com/]zawaubnjwdrj[/link], http://roicyhhumdii.com/');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (367, 46, 'k');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (368, 46, 'c');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (369, 46, 'h');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (370, 46, 'f');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (371, 46, 'd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (372, 46, 'b');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (373, 46, 'a');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (374, 47, 'Bug Fix 3');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (375, 47, 'Last Second Revision 4');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (376, 47, 'Style Change 2');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (377, 47, 'Bug Fix 6');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (378, 47, 'Feature 12');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (379, 47, 'Feature 1');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (380, 48, 'jsdfjoisdflkj');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (381, 48, 'jojiojsdf');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (382, 48, 'dfsfasd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (383, 48, 'sdfa');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (384, 48, 'ncd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (385, 48, 'bravo');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (386, 48, 'albph');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (387, 49, '•	My monastic pursuits were cut short after I removed all the ''i'' and ''e'' plates from the monastery print shop. The argument that I had taken the vowels of silence fell on deaf ears, and I was asked to leave. ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (388, 49, '•	Modern technologies, like texting and voice recognition, allow me to miscommunicate with greater efficiency. ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (389, 49, '•	I just placed a row of lawn chairs in front of our house in hopes of attracting a parade ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (390, 49, '•	When I was five, I was kicked out of VBS for drawing a picture on the wall using the salisbury steak they served at lunch. Officially, I was removed for violating rule #2 - having made a gravy image. ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (391, 49, '•	Rash decisions are best left to dermatologists. ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (392, 49, '•	My shipment of religious sheet music is now three weeks late. That''s what I get for hiring Hymn Hauling Around.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (393, 49, '•	Support government workers, because the internet won''t surf itself (yet). ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (394, 49, '•	Heading to the mortuary later today to pick up the cremains of my donkey. I''m really looking forward to having my ass handed to me. ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (396, 50, 'Battleship');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (397, 50, 'The Raid');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (398, 50, 'Men in Black III');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (399, 50, 'The Raven');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (400, 50, 'The Grey');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (401, 50, 'The Woman in Black');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (402, 50, 'Gangster Squad');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (403, 50, 'Gravity');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (404, 50, 'Argo');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (405, 50, 'Moonrise Kingdom');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (406, 50, 'Prometheus');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (407, 50, 'Gambit');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (408, 50, 'Looper');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (409, 50, 'Dredd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (410, 50, 'The Bourne legacy ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (411, 50, 'Ted ');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (412, 50, 'Dark shadows');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (413, 50, 'Django unchained');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (414, 50, 'Abe Lincoln vampire hunter');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (415, 50, 'Expendables 2');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (416, 50, 'Ddl lincoln');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (417, 50, 'Brave');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (418, 50, 'The hobbit');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (419, 50, 'Tomorrow');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (420, 50, 'Seven psychopaths');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (421, 50, 'the master');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (422, 50, 'James bond 23');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (423, 50, 'Spider man');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (424, 50, 'The avengers');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (425, 50, 'The dark knight rises');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (426, 51, 'DW Client Node');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (427, 51, 'GT3 Nimsoft');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (428, 51, 'GT3 SCOM');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (429, 52, 'Lack of collaboration software');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (430, 52, 'Lack of development experience');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (431, 52, 'Lack of unix skills');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (432, 52, 'English communication skills');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (433, 52, 'Lack timely communication');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (434, 52, 'Timezone differences');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (435, 53, 'd');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (436, 53, 'c');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (437, 53, 'b');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (438, 53, 'a');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (439, 54, '');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (440, 54, 'As a contribution receiver, I want automatic demodulator configuration so that my operators have ease of use.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (441, 54, 'As a contribution receiver, I want IP transport so that I can take advantage of lower cost trasmission bandwidth.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (442, 54, 'As a cable operator, I want IP transport for ad insertion so that I can handle more i/o than ASI.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (443, 54, 'As a distributor, I want temp keys to assist my collections effort.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (444, 54, 'As a contribution receiver, I want LPCM passthru because I think the bandwidth is cheap.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (445, 54, 'As a contribution receiver, I want AC3 decode because I think the quality is better.');
INSERT INTO rankalist_listitem (id, related_list_id, text) VALUES (446, 54, 'As a provider of codistribution content, I want receiver entitlements so I can control who receives my content.');


--
-- Data for Name: rankalist_voter; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (1, 'Test', 1);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (2, 'test2', 1);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (3, 'Marc', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (4, 'adsfs', 3);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (5, 'Marc #2', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (6, 'OMG FIX EVERYTHING!!!!!1', 4);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (7, 'Marc', 5);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (8, 'Kelly', 5);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (9, 'g', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (10, 'Marc', 6);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (11, 'pepe', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (12, 'b', 7);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (13, 'G', 9);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (14, 'ie7 test', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (16, 'Michael Casey - first pass', 12);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (17, 'Colleen', 12);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (18, 'nnn', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (19, 'John Rambo', 14);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (20, 'Foo', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (21, 'fabian', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (22, 'Steve', 16);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (23, 'Cindy', 12);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (24, 'APP B', 17);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (25, 'buki', 18);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (26, 'Test', 20);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (27, 'Ipad', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (28, 'foo', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (29, 'buzzer', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (30, 'sdsdsdfsdf', 22);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (31, 'test', 24);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (32, 'Cindy De Zitter', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (33, 'Sudar', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (34, 'sudar', 26);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (35, 'PG', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (36, 'PPM-Support', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (37, 'AR', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (38, 'Joachim', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (39, 're', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (40, 'Jörg', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (41, 'MR.X', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (42, 'TM', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (43, 'DH', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (44, 'TR', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (45, 'sku', 27);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (46, 'test', 30);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (47, 'Author', 34);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (48, 'rrrrrrrrrrr rrrrrrrrrrrrrrrrrrrrrrr', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (49, 'gh', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (50, 'Lord Justice', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (51, 'Raiu', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (52, 'Sissel', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (53, 'Gordreg', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (54, 'Nex Terren', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (55, 'Nalray', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (56, 'Rob', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (57, 'test', 37);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (58, 'Foo', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (59, 'Foo', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (60, 'gs', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (61, 'Ballu', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (62, 'fdsf', 48);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (63, 'Craig', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (64, 'test', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (65, 'Pavel', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (66, 'Silvertower', 35);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (67, 'idonthaveone', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (68, 'Ken L.', 51);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (69, 'Ga', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (70, 'roseyNme', 52);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (71, 'Jeremy', 2);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (72, 'Jeremy', 54);
INSERT INTO rankalist_voter (id, name, related_list_id) VALUES (73, 'marc', 2);


--
-- Data for Name: rankalist_vote; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (1, 1, 1, 6, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (2, 1, 2, 13, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (3, 1, 3, 15, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (4, 1, 4, 16, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (5, 1, 5, 9, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (6, 1, 6, 11, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (7, 1, 7, 12, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (8, 1, 8, 14, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (9, 1, 9, 8, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (10, 1, 10, 5, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (11, 1, 11, 7, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (12, 1, 12, 10, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (13, 1, 13, 4, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (14, 1, 14, 3, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (15, 1, 15, 2, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (16, 1, 16, 1, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (17, 2, 1, 7, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (18, 2, 2, 14, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (19, 2, 3, 15, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (20, 2, 4, 16, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (21, 2, 5, 1, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (22, 2, 6, 2, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (23, 2, 7, 3, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (24, 2, 8, 4, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (25, 2, 9, 5, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (26, 2, 10, 8, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (27, 2, 11, 11, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (28, 2, 12, 12, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (29, 2, 13, 13, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (30, 2, 14, 9, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (31, 2, 15, 10, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (32, 2, 16, 6, 1);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (33, 3, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (34, 3, 2, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (35, 3, 3, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (36, 3, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (37, 4, 1, 30, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (38, 4, 2, 25, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (39, 4, 3, 31, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (40, 4, 4, 23, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (41, 4, 5, 29, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (42, 4, 6, 22, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (43, 4, 7, 28, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (44, 4, 8, 24, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (45, 4, 9, 27, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (46, 4, 10, 21, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (47, 4, 11, 32, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (48, 4, 12, 33, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (49, 4, 13, 26, 3);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (50, 5, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (51, 5, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (52, 5, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (53, 5, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (54, 6, 1, 37, 4);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (55, 6, 2, 34, 4);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (56, 6, 3, 38, 4);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (57, 6, 4, 35, 4);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (58, 6, 5, 36, 4);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (59, 7, 1, 66, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (60, 7, 2, 65, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (61, 7, 3, 55, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (62, 7, 4, 56, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (63, 7, 5, 60, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (64, 7, 6, 59, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (65, 7, 7, 63, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (66, 7, 8, 64, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (67, 7, 9, 53, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (68, 7, 10, 58, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (69, 7, 11, 54, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (70, 7, 12, 62, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (71, 7, 13, 52, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (72, 7, 14, 57, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (73, 7, 15, 45, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (74, 7, 16, 48, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (75, 7, 17, 47, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (76, 7, 18, 40, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (77, 7, 19, 61, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (78, 7, 20, 44, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (79, 7, 21, 39, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (80, 7, 22, 49, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (81, 7, 23, 51, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (82, 7, 24, 50, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (83, 7, 25, 46, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (84, 7, 26, 42, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (85, 7, 27, 41, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (86, 7, 28, 43, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (87, 8, 1, 66, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (88, 8, 2, 56, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (89, 8, 3, 55, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (90, 8, 4, 60, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (91, 8, 5, 65, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (92, 8, 6, 59, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (93, 8, 7, 58, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (94, 8, 8, 54, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (95, 8, 9, 53, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (96, 8, 10, 64, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (97, 8, 11, 52, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (98, 8, 12, 47, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (99, 8, 13, 48, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (100, 8, 14, 49, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (101, 8, 15, 63, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (102, 8, 16, 62, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (103, 8, 17, 40, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (104, 8, 18, 39, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (105, 8, 19, 50, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (106, 8, 20, 51, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (107, 8, 21, 41, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (108, 8, 22, 42, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (109, 8, 23, 61, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (110, 8, 24, 46, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (111, 8, 25, 44, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (112, 8, 26, 45, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (113, 8, 27, 57, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (114, 8, 28, 43, 5);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (115, 9, 1, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (116, 9, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (117, 9, 3, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (118, 9, 4, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (119, 10, 1, 100, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (120, 10, 2, 96, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (121, 10, 3, 98, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (122, 10, 4, 99, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (123, 10, 5, 102, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (124, 10, 6, 97, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (125, 10, 7, 92, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (126, 10, 8, 89, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (127, 10, 9, 84, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (128, 10, 10, 79, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (129, 10, 11, 101, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (130, 10, 12, 95, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (131, 10, 13, 94, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (132, 10, 14, 93, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (133, 10, 15, 91, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (134, 10, 16, 90, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (135, 10, 17, 88, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (136, 10, 18, 87, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (137, 10, 19, 86, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (138, 10, 20, 85, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (139, 10, 21, 83, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (140, 10, 22, 82, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (141, 10, 23, 81, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (142, 10, 24, 80, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (143, 10, 25, 78, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (144, 10, 26, 77, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (145, 10, 27, 76, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (146, 10, 28, 75, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (147, 10, 29, 74, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (148, 10, 30, 73, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (149, 10, 31, 72, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (150, 10, 32, 71, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (151, 10, 33, 70, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (152, 10, 34, 69, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (153, 10, 35, 68, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (154, 10, 36, 67, 6);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (155, 11, 1, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (156, 11, 2, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (157, 11, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (158, 11, 4, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (159, 12, 1, 106, 7);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (160, 12, 2, 105, 7);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (161, 12, 3, 104, 7);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (162, 12, 4, 103, 7);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (163, 13, 1, 145, 9);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (164, 13, 2, 144, 9);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (165, 13, 3, 146, 9);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (166, 13, 4, 143, 9);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (167, 14, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (168, 14, 2, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (169, 14, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (170, 14, 4, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (208, 16, 1, 193, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (209, 16, 2, 191, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (210, 16, 3, 181, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (211, 16, 4, 182, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (212, 16, 5, 185, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (213, 16, 6, 186, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (214, 16, 7, 178, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (215, 16, 8, 180, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (216, 16, 9, 163, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (217, 16, 10, 188, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (218, 16, 11, 167, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (219, 16, 12, 168, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (220, 16, 13, 187, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (221, 16, 14, 174, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (222, 16, 15, 173, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (223, 16, 16, 172, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (224, 16, 17, 169, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (225, 16, 18, 189, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (226, 16, 19, 192, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (227, 16, 20, 179, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (228, 16, 21, 164, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (229, 16, 22, 158, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (230, 16, 23, 171, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (231, 16, 24, 170, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (232, 16, 25, 175, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (233, 16, 26, 176, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (234, 16, 27, 177, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (235, 16, 28, 166, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (236, 16, 29, 165, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (237, 16, 30, 190, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (238, 16, 31, 184, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (239, 16, 32, 183, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (240, 16, 33, 161, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (241, 16, 34, 160, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (242, 16, 35, 159, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (243, 16, 36, 157, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (244, 16, 37, 162, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (245, 17, 1, 193, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (246, 17, 2, 191, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (247, 17, 3, 186, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (248, 17, 4, 180, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (249, 17, 5, 181, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (250, 17, 6, 182, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (251, 17, 7, 185, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (252, 17, 8, 184, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (253, 17, 9, 183, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (254, 17, 10, 163, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (255, 17, 11, 166, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (256, 17, 12, 190, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (257, 17, 13, 189, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (258, 17, 14, 188, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (259, 17, 15, 192, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (260, 17, 16, 179, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (261, 17, 17, 178, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (262, 17, 18, 167, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (263, 17, 19, 168, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (264, 17, 20, 171, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (265, 17, 21, 170, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (266, 17, 22, 169, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (267, 17, 23, 187, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (268, 17, 24, 165, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (269, 17, 25, 162, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (270, 17, 26, 161, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (271, 17, 27, 160, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (272, 17, 28, 177, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (273, 17, 29, 159, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (274, 17, 30, 157, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (275, 17, 31, 173, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (276, 17, 32, 174, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (277, 17, 33, 164, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (278, 17, 34, 172, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (279, 17, 35, 158, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (280, 17, 36, 175, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (281, 17, 37, 176, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (282, 18, 1, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (283, 18, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (284, 18, 3, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (285, 18, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (286, 19, 1, 203, 14);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (287, 19, 2, 200, 14);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (288, 19, 3, 201, 14);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (289, 19, 4, 202, 14);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (290, 19, 5, 199, 14);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (291, 20, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (292, 20, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (293, 20, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (294, 20, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (295, 21, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (296, 21, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (297, 21, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (298, 21, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (299, 22, 1, 206, 16);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (300, 22, 2, 205, 16);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (301, 22, 3, 207, 16);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (302, 23, 1, 163, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (303, 23, 2, 193, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (304, 23, 3, 192, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (305, 23, 4, 191, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (306, 23, 5, 189, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (307, 23, 6, 166, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (308, 23, 7, 188, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (309, 23, 8, 186, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (310, 23, 9, 185, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (311, 23, 10, 184, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (312, 23, 11, 182, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (313, 23, 12, 181, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (314, 23, 13, 160, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (315, 23, 14, 165, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (316, 23, 15, 179, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (317, 23, 16, 178, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (318, 23, 17, 162, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (319, 23, 18, 161, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (320, 23, 19, 180, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (321, 23, 20, 183, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (322, 23, 21, 174, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (323, 23, 22, 173, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (324, 23, 23, 172, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (325, 23, 24, 168, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (326, 23, 25, 167, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (327, 23, 26, 157, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (328, 23, 27, 164, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (329, 23, 28, 187, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (330, 23, 29, 169, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (331, 23, 30, 171, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (332, 23, 31, 170, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (333, 23, 32, 175, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (334, 23, 33, 177, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (335, 23, 34, 176, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (336, 23, 35, 190, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (337, 23, 36, 159, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (338, 23, 37, 158, 12);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (339, 24, 1, 212, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (340, 24, 2, 214, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (341, 24, 3, 215, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (342, 24, 4, 208, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (343, 24, 5, 210, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (344, 24, 6, 211, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (345, 24, 7, 213, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (346, 24, 8, 209, 17);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (347, 25, 1, 231, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (348, 25, 2, 228, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (349, 25, 3, 223, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (350, 25, 4, 225, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (351, 25, 5, 227, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (352, 25, 6, 226, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (353, 25, 7, 229, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (354, 25, 8, 230, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (355, 25, 9, 217, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (356, 25, 10, 219, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (357, 25, 11, 220, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (358, 25, 12, 222, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (359, 25, 13, 216, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (360, 25, 14, 218, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (361, 25, 15, 224, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (362, 25, 16, 221, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (363, 25, 17, 232, 18);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (364, 26, 1, 240, 20);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (365, 26, 2, 237, 20);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (366, 26, 3, 239, 20);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (367, 26, 4, 241, 20);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (368, 26, 5, 235, 20);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (369, 26, 6, 238, 20);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (370, 26, 7, 236, 20);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (371, 27, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (372, 27, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (373, 27, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (374, 27, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (375, 28, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (376, 28, 2, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (377, 28, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (378, 28, 4, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (379, 29, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (380, 29, 2, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (381, 29, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (382, 29, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (383, 30, 1, 246, 22);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (384, 31, 1, 252, 24);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (385, 31, 2, 251, 24);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (386, 31, 3, 250, 24);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (387, 32, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (388, 32, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (389, 32, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (390, 32, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (391, 33, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (392, 33, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (393, 33, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (394, 33, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (395, 34, 1, 254, 26);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (396, 35, 1, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (397, 35, 2, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (398, 35, 3, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (399, 35, 4, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (400, 35, 5, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (401, 35, 6, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (402, 35, 7, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (403, 36, 1, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (404, 36, 2, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (405, 36, 3, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (406, 36, 4, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (407, 36, 5, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (408, 36, 6, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (409, 36, 7, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (410, 37, 1, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (411, 37, 2, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (412, 37, 3, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (413, 37, 4, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (414, 37, 5, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (415, 37, 6, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (416, 37, 7, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (417, 38, 1, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (418, 38, 2, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (419, 38, 3, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (420, 38, 4, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (421, 38, 5, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (422, 38, 6, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (423, 38, 7, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (424, 39, 1, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (425, 39, 2, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (426, 39, 3, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (427, 39, 4, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (428, 39, 5, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (429, 39, 6, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (430, 39, 7, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (431, 40, 1, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (432, 40, 2, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (433, 40, 3, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (434, 40, 4, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (435, 40, 5, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (436, 40, 6, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (437, 40, 7, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (438, 41, 1, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (439, 41, 2, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (440, 41, 3, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (441, 41, 4, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (442, 41, 5, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (443, 41, 6, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (444, 41, 7, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (445, 42, 1, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (446, 42, 2, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (447, 42, 3, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (448, 42, 4, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (449, 42, 5, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (450, 42, 6, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (451, 42, 7, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (452, 43, 1, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (453, 43, 2, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (454, 43, 3, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (455, 43, 4, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (456, 43, 5, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (457, 43, 6, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (458, 43, 7, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (459, 44, 1, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (460, 44, 2, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (461, 44, 3, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (462, 44, 4, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (463, 44, 5, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (464, 44, 6, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (465, 44, 7, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (466, 45, 1, 259, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (467, 45, 2, 257, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (468, 45, 3, 258, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (469, 45, 4, 260, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (470, 45, 5, 261, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (471, 45, 6, 256, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (472, 45, 7, 255, 27);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (473, 46, 1, 270, 30);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (474, 46, 2, 268, 30);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (475, 46, 3, 269, 30);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (476, 47, 1, 289, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (477, 47, 2, 286, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (478, 47, 3, 288, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (479, 47, 4, 287, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (480, 47, 5, 285, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (481, 47, 6, 291, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (482, 47, 7, 290, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (483, 47, 8, 292, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (484, 47, 9, 298, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (485, 47, 10, 299, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (486, 47, 11, 302, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (487, 47, 12, 301, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (488, 47, 13, 300, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (489, 47, 14, 295, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (490, 47, 15, 294, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (491, 47, 16, 293, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (492, 47, 17, 297, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (493, 47, 18, 296, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (494, 47, 19, 284, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (495, 47, 20, 283, 34);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (496, 48, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (497, 48, 2, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (498, 48, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (499, 48, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (500, 49, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (501, 49, 2, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (502, 49, 3, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (503, 49, 4, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (504, 50, 1, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (505, 50, 2, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (506, 50, 3, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (507, 50, 4, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (508, 50, 5, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (509, 51, 1, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (510, 51, 2, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (511, 51, 3, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (512, 51, 4, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (513, 51, 5, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (514, 52, 1, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (515, 52, 2, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (516, 52, 3, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (517, 52, 4, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (518, 52, 5, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (519, 53, 1, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (520, 53, 2, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (521, 53, 3, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (522, 53, 4, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (523, 53, 5, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (524, 54, 1, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (525, 54, 2, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (526, 54, 3, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (527, 54, 4, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (528, 54, 5, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (529, 55, 1, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (530, 55, 2, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (531, 55, 3, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (532, 55, 4, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (533, 55, 5, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (534, 56, 1, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (535, 56, 2, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (536, 56, 3, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (537, 56, 4, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (538, 56, 5, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (539, 57, 1, 313, 37);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (540, 57, 2, 311, 37);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (541, 57, 3, 312, 37);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (542, 57, 4, 310, 37);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (543, 57, 5, 309, 37);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (544, 58, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (545, 58, 2, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (546, 58, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (547, 58, 4, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (548, 59, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (549, 59, 2, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (550, 59, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (551, 59, 4, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (552, 60, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (553, 60, 2, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (554, 60, 3, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (555, 60, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (556, 61, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (557, 61, 2, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (558, 61, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (559, 61, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (560, 62, 1, 386, 48);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (561, 62, 2, 382, 48);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (562, 62, 3, 385, 48);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (563, 62, 4, 381, 48);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (564, 62, 5, 383, 48);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (565, 62, 6, 384, 48);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (566, 62, 7, 380, 48);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (567, 63, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (568, 63, 2, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (569, 63, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (570, 63, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (571, 64, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (572, 64, 2, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (573, 64, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (574, 64, 4, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (575, 65, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (576, 65, 2, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (577, 65, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (578, 65, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (579, 66, 1, 306, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (580, 66, 2, 305, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (581, 66, 3, 307, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (582, 66, 4, 304, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (583, 66, 5, 303, 35);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (584, 67, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (585, 67, 2, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (586, 67, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (587, 67, 4, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (588, 68, 1, 428, 51);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (589, 68, 2, 427, 51);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (590, 68, 3, 426, 51);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (591, 69, 1, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (592, 69, 2, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (593, 69, 3, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (594, 69, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (595, 70, 1, 434, 52);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (596, 70, 2, 431, 52);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (597, 70, 3, 430, 52);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (598, 70, 4, 429, 52);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (599, 70, 5, 433, 52);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (600, 70, 6, 432, 52);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (601, 71, 1, 18, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (602, 71, 2, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (603, 71, 3, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (604, 71, 4, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (605, 72, 1, 446, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (606, 72, 2, 443, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (607, 72, 3, 441, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (608, 72, 4, 444, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (609, 72, 5, 445, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (610, 72, 6, 440, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (611, 72, 7, 442, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (612, 72, 8, 439, 54);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (613, 73, 1, 19, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (614, 73, 2, 20, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (615, 73, 3, 17, 2);
INSERT INTO rankalist_vote (id, voter_id, rank, item_id, related_list_id) VALUES (616, 73, 4, 18, 2);


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: rankalist
--

INSERT INTO south_migrationhistory (id, app_name, migration, applied) VALUES (1, 'rankalist', '0001_initial', '2011-10-12 20:18:27.671288-04');
INSERT INTO south_migrationhistory (id, app_name, migration, applied) VALUES (2, 'rankalist', '0002_auto__del_field_list_name', '2011-10-12 20:18:27.705995-04');
INSERT INTO south_migrationhistory (id, app_name, migration, applied) VALUES (3, 'rankalist', '0003_auto__add_field_vote_item', '2011-10-12 21:05:55.885695-04');
INSERT INTO south_migrationhistory (id, app_name, migration, applied) VALUES (4, 'rankalist', '0004_auto__add_field_vote_related_list', '2011-10-12 21:05:55.946532-04');


--
-- PostgreSQL database dump complete
--

