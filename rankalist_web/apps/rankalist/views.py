from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.conf import settings
import random
import string
import logging

from models import *
from forms import *


logger = logging.getLogger(__name__)

def results(request, list_id):
    l = List.objects.get(adminKey=list_id)
    items = []
    for item in l.items.all():
        total = 0
        count = 0
        for vote in item.votes.all():
            total += vote.rank
            count += 1

        if count > 0:
            items.append( (total, item) )
    items.sort(key=lambda a: a[0])
    items = [i[1] for i in items]
    return render_to_response("rankalist/results.html", { "averages":items, "list":l },
                                                 context_instance=RequestContext(request))

def vote(request, list_id):
    l = List.objects.get(key = list_id)

    if request.method == "POST":
        form = RankForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            orders = request.POST.get("orders")
            orders = orders.split(" ")
            
            voter = Voter(related_list=l, name=name)
            voter.save()
            
            rank = 0
            for order in orders:        
                if order == "":
                    continue
                rank += 1
                listItem = ListItem.objects.get(id=order)
                if listItem.related_list == l:
                    v = Vote(related_list=l, voter=voter, rank=rank, item=listItem)
                    v.save()                
            return HttpResponseRedirect("%s?key=%s" % (reverse('thanks'), l.key ))
    else:
        form = RankForm()
    return render_to_response("rankalist/vote.html", { "form":form, "list":l },
                                                 context_instance=RequestContext(request))
def thanks(request):
    return render_to_response("rankalist/thanks.html",  context_instance=RequestContext(request))

# def clone(request, list_id):
    
    
    
def home(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid(): # All validation rules pass
            k, ak = _generateKeys()
            l = List(key=k, adminKey=ak, instructions=form.cleaned_data['instructions'])
            l.save()
            items = form.cleaned_data['items'].splitlines()
            items.reverse()
            for item in items:
                li = ListItem(related_list=l, text=item)
                li.save()                
            
            return HttpResponseRedirect(reverse('confirm',args=[l.adminKey]))
    else:
        form = ListForm()
    return render_to_response("rankalist/home.html", { "form":form },
                                                              context_instance=RequestContext(request))

def _generateKeys():
    ak = ''.join(random.choice(string.letters + string.digits) for i in xrange(8))
    k = ''.join(random.choice(string.letters + string.digits) for i in xrange(8))
    try:
        List.objects.get(key=k, adminKey=ak)
    except:
        return (k, ak)
    return _generateKeys()
    

def confirm(request, list_id):
    l = List.objects.get(adminKey = list_id)
    return render_to_response("rankalist/confirm.html", { 'list':l },
                                                              context_instance=RequestContext(request))
    