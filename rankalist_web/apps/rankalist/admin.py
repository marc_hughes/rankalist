from django.contrib import admin
from models import *

# 
# class BasecampAccountAdmin(admin.ModelAdmin):
#     list_display = ('account_name', 'account_id','subscription_type','subscription_expiration')
#     
# admin.site.register(BasecampAccount,BasecampAccountAdmin)


admin.site.register(List)
admin.site.register(ListItem)
admin.site.register(Voter)
admin.site.register(Vote)
