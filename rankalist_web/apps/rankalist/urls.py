from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('rankalist.views',
    url(r'^$', "home", name="home"),
    url(r'^thanks$', "thanks", name="thanks"),
    url(r'^confirm/(?P<list_id>.+)$', "confirm", name="confirm"),
    url(r'^vote/(?P<list_id>.+)$', "vote", name="vote"),
    url(r'^results/(?P<list_id>.+)$', "results", name="results"),            
    url(r'^robots.txt', direct_to_template, {'template': 'robots.txt'}),
)
