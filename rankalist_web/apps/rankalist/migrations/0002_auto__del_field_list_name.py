# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'List.name'
        db.delete_column('rankalist_list', 'name')


    def backwards(self, orm):
        
        # Adding field 'List.name'
        db.add_column('rankalist_list', 'name', self.gf('django.db.models.fields.CharField')(default='', max_length=128), keep_default=False)


    models = {
        'rankalist.list': {
            'Meta': {'object_name': 'List'},
            'adminKey': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instructions': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '8'})
        },
        'rankalist.listitem': {
            'Meta': {'object_name': 'ListItem'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'related_list': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': "orm['rankalist.List']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'rankalist.vote': {
            'Meta': {'object_name': 'Vote'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rank': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'voter': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'votes'", 'to': "orm['rankalist.Voter']"})
        },
        'rankalist.voter': {
            'Meta': {'object_name': 'Voter'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'related_list': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'voters'", 'to': "orm['rankalist.List']"})
        }
    }

    complete_apps = ['rankalist']
