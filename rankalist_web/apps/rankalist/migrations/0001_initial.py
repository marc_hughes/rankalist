# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'List'
        db.create_table('rankalist_list', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('adminKey', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('instructions', self.gf('django.db.models.fields.CharField')(max_length=512)),
        ))
        db.send_create_signal('rankalist', ['List'])

        # Adding model 'ListItem'
        db.create_table('rankalist_listitem', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('related_list', self.gf('django.db.models.fields.related.ForeignKey')(related_name='items', to=orm['rankalist.List'])),
            ('text', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('rankalist', ['ListItem'])

        # Adding model 'Voter'
        db.create_table('rankalist_voter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('related_list', self.gf('django.db.models.fields.related.ForeignKey')(related_name='voters', to=orm['rankalist.List'])),
        ))
        db.send_create_signal('rankalist', ['Voter'])

        # Adding model 'Vote'
        db.create_table('rankalist_vote', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('voter', self.gf('django.db.models.fields.related.ForeignKey')(related_name='votes', to=orm['rankalist.Voter'])),
            ('rank', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('rankalist', ['Vote'])


    def backwards(self, orm):
        
        # Deleting model 'List'
        db.delete_table('rankalist_list')

        # Deleting model 'ListItem'
        db.delete_table('rankalist_listitem')

        # Deleting model 'Voter'
        db.delete_table('rankalist_voter')

        # Deleting model 'Vote'
        db.delete_table('rankalist_vote')


    models = {
        'rankalist.list': {
            'Meta': {'object_name': 'List'},
            'adminKey': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'instructions': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        'rankalist.listitem': {
            'Meta': {'object_name': 'ListItem'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'related_list': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'items'", 'to': "orm['rankalist.List']"}),
            'text': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'rankalist.vote': {
            'Meta': {'object_name': 'Vote'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rank': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'voter': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'votes'", 'to': "orm['rankalist.Voter']"})
        },
        'rankalist.voter': {
            'Meta': {'object_name': 'Voter'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'related_list': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'voters'", 'to': "orm['rankalist.List']"})
        }
    }

    complete_apps = ['rankalist']
