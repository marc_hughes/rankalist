from django.db import models

# Create your models here.
class List( models.Model ):
    key = models.CharField(max_length=8)
    adminKey = models.CharField(max_length=8)
    instructions = models.CharField(max_length=512)

class ListItem(models.Model):
    related_list = models.ForeignKey(List, related_name="items")
    text = models.CharField(max_length=256)
    class Meta:
        ordering = ('-id',)
        
class Voter(models.Model):
    name = models.CharField(max_length=128)
    related_list = models.ForeignKey(List, related_name="voters")

class Vote(models.Model):
    related_list = models.ForeignKey(List, related_name="votes")
    item = models.ForeignKey(ListItem, related_name="votes")
    voter = models.ForeignKey(Voter, related_name="votes")
    rank = models.IntegerField(default=0)
    class Meta:
        ordering = ('rank',)