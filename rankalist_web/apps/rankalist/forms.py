from django import forms
from django.core.urlresolvers import reverse


class ListForm(forms.Form):    
    instructions = forms.CharField(widget=forms.Textarea,initial="Please rank these items in order of importance.", help_text="Anyone ranking this list will see these instructions.")
    items = forms.CharField(widget=forms.Textarea,help_text="Enter one item per line.")

class RankForm(forms.Form):
    name = forms.CharField()