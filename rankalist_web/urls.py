from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rankalist_web.views.home', name='home'),
    # url(r'^rankalist_web/', include('rankalist_web.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include("rankalist.urls")),
    url(r'^favicon.ico', 'django.views.generic.simple.redirect_to', {'url': 'https://s3.amazonaws.com/projectite-cdn/favicon.ico'} ),    
)
